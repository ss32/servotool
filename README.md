# servoTool

Python3 tool to troubleshoot servos for the [creepyCam](https://gitlab.com/ss32/creepycam) project.

---

## How Do

Install [pyFirmata](https://pypi.org/project/pyFirmata/) and [PyYaml](https://pypi.org/project/PyYAML/) with `python3 -m pip install pyfirmata pyyaml --user`.  The Arduino software is bundled by default in the Arduino IDE and is available under `File-->Examples-->Firmata-->StandardFirmata`.

Run `python3 servo_tool.py` and command options are printed to the terminal.

```
Found Arduino at /dev/ttyUSB0

Commands are issued as: <pin> <servo_position>
Pin options are: x - eye x axis
                 y - eye y axis
                 b - bottom eyelid
                 t - top eyelid
Example: x 125
Moves the X-axis servo to 125 degrees

Type exit as a command  or hit CTRL+C to quit
----------------------------------------------
Command: x 95
Command: exit
Exiting
```