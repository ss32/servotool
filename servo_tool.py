from pyfirmata import SERVO
import pyfirmata
from time import sleep
import zmq
import sys
import yaml


def get_config():
    with open("config.yaml") as config_file:
        config = yaml.load(config_file, Loader=yaml.FullLoader)
    return config


def arduino_setup(config):
    board = pyfirmata.Arduino("{}".format(config["board_path"]))
    print("Found Arduino at {}".format(config["board_path"]))
    # start an iterator thread so the serial buffer doesn't overflow
    iter8 = pyfirmata.util.Iterator(board)
    iter8.start()

    xpin = board.get_pin("d:{}:s".format(config["xpin"]))
    ypin = board.get_pin("d:{}:s".format(config["ypin"]))
    top_eyelid_pin = board.get_pin("d:{}:s".format(config["top_eyelid_pin"]))
    bottom_eyelid_pin = board.get_pin("d:{}:s".format(config["bottom_eyelid_pin"]))

    ypin.write(90)
    xpin.write(90)
    top_eyelid_pin.write(90)
    bottom_eyelid_pin.write(90)

    return xpin, ypin, top_eyelid_pin, bottom_eyelid_pin


def execute_command(cmd, xpin, ypin, top_eyelid_pin, bottom_eyelid_pin):

    pin = cmd[0]
    try:
        servo = int(cmd[2:])
    except ValueError:
        return False
    if pin == "x":
        xpin.write(servo)
    if pin == "y":
        ypin.write(servo)
    if pin == "t":
        top_eyelid_pin.write(servo)
    if pin == "b":
        bottom_eyelid_pin.write(servo)

    return True


def blink(config):
    top_eyelid_pin.write(90 - config["CLOSED_DIFF"])
    bottom_eyelid_pin.write(90)
    sleep(0.5)


def main():

    config = get_config()
    xpin, ypin, top_eyelid_pin, bottom_eyelid_pin = arduino_setup(config)

    print("\nCommands are issued as: <pin> <servo_position>")
    print("Pin options are: x - eye x axis")
    print("                 y - eye y axis")
    print("                 b - bottom eyelid")
    print("                 t - top eyelid")
    print("Example: x 125")
    print("Moves the X-axis servo to 125 degrees")
    print("\nType exit as a command  or hit CTRL+C to quit")
    print("----------------------------------------------")

    while True:
        try:
            cmd = input("Command: ")
            if cmd == "exit":
                print("Exiting")
                for M in ["x", "y", "b", "t"]:
                    execute_command("{} 90".format(M), xpin, ypin, top_eyelid_pin, bottom_eyelid_pin)
                sys.exit(0)
            res = execute_command(cmd, xpin, ypin, top_eyelid_pin, bottom_eyelid_pin)
            if not res:
                print("Error: Didn't understand that input.")

        except KeyboardInterrupt:
            print("Got kill signal, exiting.")
            sys.exit(0)


if __name__ == "__main__":
    main()